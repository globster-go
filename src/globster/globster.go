package main

import (
	"blicky.net/tanja"
	"flag"
	"globster/hub"
	"globster/userlist"
	"log"
	"net"
)

func listen(node *tanja.Node, path string) (e error) {
	var addr *net.UnixAddr
	if addr, e = net.ResolveUnixAddr("unix", path); e != nil {
		return
	}
	var lst *net.UnixListener
	if lst, e = net.ListenUnix("unix", addr); e != nil {
		return
	}
	i := 0
	for {
		var conn net.Conn
		if conn, e = lst.Accept(); e != nil {
			return
		}
		i++
		log.Printf("%03d: Incoming connection on UNIX socket.", i)
		lnk := node.Link(conn)
		go func(i int) {
			err := <-lnk.Start()
			if err == nil {
				log.Printf("%03d: Disconnected.", i)
			} else {
				log.Printf("%03d: %s", i, err)
			}
		}(i)
	}
	return
}

func main() {
	path := flag.String("l", "/tmp/globster.sock", "Path to the UNIX listen socket.")
	flag.Parse()

	n := tanja.NewNode()

	hub := hub.New(n)
	go hub.Run()

	userlist := userlist.New(n)
	go userlist.Run()

	if err := listen(n, *path); err != nil {
		log.Fatalf("Error listening on UNIX socket: %s", err)
	}
}
