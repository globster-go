package adc

import (
	"bufio"
	"errors"
	"io"
	"unicode/utf8"
)

func ParseArg(b []byte) (Arg, []byte, error) {
	var res []byte
	for len(b) > 0 {
		if b[0] == ' ' || b[0] == '\n' {
			break
		}
		if b[0] == '\\' {
			if len(b) < 2 {
				return "", b, errors.New("Unexpected end of message after escape.")
			}
			if b[1] == '\\' {
				res = append(res, '\\')
			} else if b[1] == 'n' {
				res = append(res, '\n')
			} else if b[1] == 's' {
				res = append(res, ' ')
			} else {
				return "", b, errors.New("Invalid escape character.")
			}
			b = b[2:]
		} else {
			res = append(res, b[0])
			b = b[1:]
		}
	}
	if !utf8.Valid(res) {
		return "", b, errors.New("Invalid UTF-8 sequence.")
	}
	return Arg(res), b, nil
}

// TODO: features
// Note: Can return nil, nil if the message is empty (which is a valid ADC message).
func Parse(buf []byte) (*Message, error) {
	if len(buf) == 1 && buf[0] == '\n' {
		return nil, nil
	}
	m := &Message{}
	if len(buf) < 4 {
		return nil, errors.New("Invalid command.")
	}
	if buf[len(buf)-1] != '\n' {
		return nil, errors.New("Message does not end with a newline.")
	}
	buf = buf[:len(buf)-1]

	// Header
	if buf[0] != 'B' && buf[0] != 'C' && buf[0] != 'I' && buf[0] != 'H' && buf[0] != 'D' && buf[0] != 'E' && buf[0] != 'F' && buf[0] != 'U' {
		return nil, errors.New("Invalid command type.")
	}
	if !isAlphaNum(buf[1]) || !isAlphaNum(buf[2]) || !isAlphaNum(buf[3]) {
		return nil, errors.New("Invalid command.")
	}
	m.Header = NewFourCC(buf)
	t := m.Header.Type()
	nfo, ok := cmdList[m.Header.Command()]
	if !ok {
		return nil, errors.New("Invalid command.")
	}
	buf = buf[4:]

	// Src
	if t == 'B' || t == 'D' || t == 'E' || t == 'F' {
		if len(buf) < 5 || buf[0] != ' ' || !isSID(buf[1:]) {
			return nil, errors.New("Invalid SID argument.")
		}
		m.Src = NewFourCC(buf[1:])
		buf = buf[5:]
	}

	// Dest
	if t == 'D' || t == 'E' {
		if len(buf) < 5 || buf[0] != ' ' || !isSID(buf[1:]) {
			return nil, errors.New("Invalid SID argument.")
		}
		m.Dest = NewFourCC(buf[1:])
		buf = buf[5:]
	}

	// CID
	if t == 'U' {
		if len(buf) < 2 || buf[0] != ' ' {
			return nil, errors.New("Invalid CID argument.")
		}
		buf = buf[1:]
		var cid []byte
		for len(buf) > 0 && buf[0] != ' ' {
			if !isBase32(buf[0]) {
				return nil, errors.New("Invalid CID argument.")
			}
			cid = append(cid, buf[0])
			buf = buf[1:]
		}
		m.CID = string(cid)
	}

	// Positional arguments
	n := nfo.args
	for n > 0 {
		if len(buf) < 1 || buf[0] != ' ' {
			return nil, errors.New("Invalid or missing positional argument.")
		}
		var s Arg
		var e error
		s, buf, e = ParseArg(buf[1:])
		if e != nil {
			return nil, errors.New("Invalid positional argument: " + e.Error())
		}
		m.PosArgs = append(m.PosArgs, s)
		n--
	}

	// Named arguments
	for len(buf) > 0 {
		if len(buf) < 3 || buf[0] != ' ' || !isAlpha(buf[1]) || !isAlphaNum(buf[2]) {
			return nil, errors.New("Invalid named argument.")
		}
		if m.NamedArgs == nil {
			m.NamedArgs = make(map[[2]byte][]Arg)
		}
		name := [2]byte{buf[1], buf[2]}
		var val Arg
		var e error
		val, buf, e = ParseArg(buf[3:])
		if e != nil {
			return nil, errors.New("Invalid named argument: " + e.Error())
		}
		m.NamedArgs[name] = append(m.NamedArgs[name], val)
	}

	return m, nil
}

type Reader struct {
	rd *bufio.Reader
}

func NewReader(rd io.Reader, maxSize int) *Reader {
	return &Reader{bufio.NewReaderSize(rd, maxSize)}
}

func (rd *Reader) Read() (*Message, error) {
	l, e := rd.rd.ReadString('\n')
	if e != nil {
		return nil, e
	}
	return Parse([]byte(l))
}
