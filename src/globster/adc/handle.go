package adc

import "errors"

var (
	ErrInvalidState = errors.New("Received message in invalid state")
	ErrInvalidType  = errors.New("Message with invalid type")
	ErrInvalidArg   = errors.New("Invalid message")
)

type HubHandler interface {
	AdcSUP(ad, rm []string) error
	AdcSID(FourCC) error
}

// Assumes the message has been received from a hub. This function performs
// validation on the message and, if valid, calls the respective method in
// Handler.
func HandleHub(m *Message, state State, h HubHandler) error {
	if m == nil {
		return nil
	}
	nfo := cmdList[m.Header.Command()]
	if state != InvalidState && (state&nfo.states) == 0 {
		return ErrInvalidState
	}
	hastype := false
	for _, t := range []byte(nfo.htypes) {
		if t == m.Header.Type() {
			hastype = true
			break
		}
	}
	if !hastype {
		return ErrInvalidType
	}

	switch m.Header.Command() {
	case SUP:
		f := func(a []Arg) []string {
			r := make([]string, len(a))
			for i, v := range a {
				if v.FourCC() == 0 {
					return nil
				}
				r[i] = string(v)
			}
			return r
		}
		ad, rm := f(m.NamedArgs[[2]byte{'A', 'D'}]), f(m.NamedArgs[[2]byte{'R', 'M'}])
		if ad == nil || rm == nil {
			return ErrInvalidArg
		}
		return h.AdcSUP(ad, rm)

	case SID:
		if !m.PosArgs[0].IsSID() {
			return ErrInvalidArg
		}
		return h.AdcSID(m.PosArgs[0].FourCC())

	}
	return nil
}
