package adc

import "io"

func (a Arg) Format(b []byte) []byte {
	// Walk over the raw bytes, Unicode characters (and anything else) is
	// simply passed through unmodified.
	for i := 0; i < len(a); i++ {
		switch a[i] {
		case ' ':
			b = append(b, []byte{'\\', 's'}...)
		case '\n':
			b = append(b, []byte{'\\', 'n'}...)
		case '\\':
			b = append(b, []byte{'\\', '\\'}...)
		default:
			b = append(b, a[i])
		}
	}
	return b
}

// TODO: features
func (m *Message) Format(b []byte) []byte {
	if m == nil {
		return append(b, '\n')
	}
	b = m.Header.Format(b)
	if m.Src != 0 {
		b = append(b, ' ')
		b = m.Src.Format(b)
	}
	if m.Dest != 0 {
		b = append(b, ' ')
		b = m.Dest.Format(b)
	}
	if len(m.CID) > 0 {
		b = append(b, ' ')
		b = append(b, []byte(m.CID)...)
	}
	for _, v := range m.PosArgs {
		b = append(b, ' ')
		b = v.Format(b)
	}
	for k, l := range m.NamedArgs {
		for _, v := range l {
			b = append(b, []byte{' ', k[0], k[1]}...)
			b = v.Format(b)
		}
	}
	return append(b, 0x0a)
}

func (m *Message) String() string {
	return string(m.Format(nil))
}

type Writer struct {
	wr io.Writer
}

func NewWriter(wr io.Writer) *Writer {
	return &Writer{wr}
}

func (wr *Writer) Write(m *Message) (int, error) {
	return wr.wr.Write(m.Format(nil))
}
