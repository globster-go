package adc

import "testing"

type argv struct {
	input  string
	output string
	rem    string
	haserr bool
}

func TestParseArg(t *testing.T) {
	l := []argv{
		{"", "", "", false},
		{" 1", "", " 1", false},
		{"1", "1", "", false},
		{"1\n", "1", "\n", false},
		{"a b", "a", " b", false},
		{"a\\\\b", "a\\b", "", false},
		{"\\n\\\\\\s", "\n\\ ", "", false},
		{"\\", "", "\\", true},
		{"\\ ", "", "\\ ", true},
		{"some\\", "", "\\", true},
		{"\\e", "", "\\e", true},
		{"\xff", "", "", true},
		{"\xc9a", "", "", true},
	}
	for _, v := range l {
		out, rem, err := ParseArg([]byte(v.input))
		if string(out) != v.output {
			t.Errorf("Unexpected output: got '%s', expected '%s'", out, v.output)
		}
		if string(rem) != v.rem {
			t.Errorf("Unexpected remainder: got '%s', expected '%s'", rem, v.rem)
		}
		if (err != nil) != v.haserr {
			t.Errorf("Unexpected error: got '%s', expected %v", err, v.haserr)
		}
	}
}

type parsev struct {
	input  string
	haserr bool
}

func TestParseFmt(t *testing.T) {
	l := []parsev{
		{"", true},
		{"\n", false},
		{"HSUP\n", false},
		{" HSUP\n", true},
		{"\nHSUP\n", true},
		{"HSUP\n", false},
		{"HSuP\n", true},
		{"NSUP\n", true},
		{"HSUP ", true},
		{"HSUP", true},
		{"HUNK\n", true},
		{"HSTA 0 blah\n", false},
		{"HSTA 0\n", true},
		{"HSTA 0 blah \n", true},
		{"HSTA  blah\n", false},
		{"HSTA  \n", false},
		{"HSTA 0 \n", false},
		{"HSUP AD0 AD1\n", false},
		{"HSUP AD AD AD\n", false},
		{"HSUP 3D\n", true},
		{"HSUP A B\n", true},
		{"HSUP  AD1\n", true},
		{"BSUP 2344\n", false},
		{"BSUP  2344\n", true},
		{"BSUP 2304\n", true},
		{"BSUP 23043\n", true},
		{"BSUP 234 \n", true},
		{"BSUP 233\n", true},
		{"BSTA 2334 0 1\n", false},
		{"BSTA 2334  \n", false},
		{"BSUP\n", true},
		{"BSUP ABCD\n", false},
		{"BSUP ABcD\n", true},
		{"BSUP abcd\n", true},
		{"DSUP ABCD \n", true},
		{"DSUP ABCD 2304D\n", true},
		{"DSUP ABCD 2304\n", true},
		{"DSUP ABCD 239\n", true},
		{"DSUP ABCD 239 \n", true},
		{"DSUP ABCD  2344\n", true},
		{"DSUP ABCD 2344\n", false},
		{"DSTA ABCD 2344 0 1\n", false},
		{"DSTA ABCD 2344  \n", false},
		{"DSTA ABCD 2344 \n", true},
		{"DSTA ABCD 2344   \n", true},
		{"USUP\n", true},
		{"USUP \n", true},
		{"USUP A\n", false},
		{"USUP ABCDEFGHIJKLMNOPQRSTUVWXYZ234567\n", false},
		{"USUP 0\n", true},
		{"USTA 0  \n", true},
		{"USTA 0 a b\n", true},
		{"HSTA  a\"'\\\\\n", false},
		{"HINF RFa\"'\\\\\n", false},
	}
	for _, v := range l {
		msg, err := Parse([]byte(v.input))
		if (err != nil) != v.haserr {
			t.Errorf("Unexpected error: got '%s' for '%s'", err, v.input)
		}
		if msg != nil {
			out := msg.Format(nil)
			if string(out) != v.input {
				t.Errorf("Unexpected output: got '%s', expected '%s'", out, v.input)
			}
		}
	}
}
