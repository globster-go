package userlist

// TODO: Also test the following:
// - Notifications
// - Multiple userlists
// - More fields

import (
	"blicky.net/tanja"
	"reflect"
	"testing"
)

func retExpect(t *testing.T, id string, r *tanja.ReturnPath, ret ...tanja.Tuple) {
	defer r.Close()
	i := 0
	for v := range r.Chan() {
		if i >= len(ret) {
			t.Errorf("%s: Received reply too many: %#v", id, v)
			return
		}
		if !reflect.DeepEqual(v, ret[i]) {
			t.Errorf("%s: Incorrect reply(%d): %#v", id, i, v)
		}
		i++
	}
	if i != len(ret) {
		t.Errorf("%s: Unexpected number of replies, got %d expected %d", id, i, len(ret))
	}
}

func TestList(t *testing.T) {
	n := tanja.NewNode()
	l := New(n)
	go l.Run()

	s := n.Session()

	retExpect(t, "empty1", s.Send(true, "userlist"))
	retExpect(t, "empty2", s.Send(true, "userlist", 1))
	retExpect(t, "empty3", s.Send(true, "userlist", 1, "get"))
	retExpect(t, "empty4", s.Send(true, "userlist", 1, "get", nil))
	retExpect(t, "set1", s.Send(true, "userlist", 1, "set", 1, tanja.Map("Name", "1")))
	retExpect(t, "stats1", s.Send(true, "userlist", 1, "stats"), tanja.Tup(1, 0))
	retExpect(t, "set2", s.Send(true, "userlist", 1, "set", 2, tanja.Map("CID", "ABC", "ShareSize", 1023)))
	retExpect(t, "stats2", s.Send(true, "userlist", 1, "stats"), tanja.Tup(2, 1023))
	retExpect(t, "get1", s.Send(true, "userlist", 1, "get", 1, "Name"), tanja.Tup(tanja.Map("Name", "1")))
	retExpect(t, "set3", s.Send(true, "userlist", 1, "set", 2, tanja.Map("ShareSize", 1020)))
	retExpect(t, "stats3", s.Send(true, "userlist", 1, "stats"), tanja.Tup(2, 1020))
	retExpect(t, "set4", s.Send(true, "userlist", 1, "set", 1, tanja.Map("ShareSize", 10)))
	retExpect(t, "stats4", s.Send(true, "userlist", 1, "stats"), tanja.Tup(2, 1030))
	retExpect(t, "getid1", s.Send(true, "userlist", 1, "getid", "ABC", "ShareSize"), tanja.Tup(tanja.Map("ShareSize", 1020)))
	retExpect(t, "get5", s.Send(true, "userlist", 1, "get", nil, "ShareSize"),
		tanja.Tup(tanja.Map("ShareSize", 10)), // Order is actually undefined
		tanja.Tup(tanja.Map("ShareSize", 1020)))
	retExpect(t, "del", s.Send(true, "userlist", 1, "del", nil))
	retExpect(t, "stats5", s.Send(true, "userlist", 1, "stats"), tanja.Tup(0, 0))
}
