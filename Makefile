.PHONY: all fmt install test clean distclean play

PWD=$(shell pwd)
GOPATH:=${PWD}:${GOPATH}
OLDBIN:=${GOBIN}
GOBIN:=${PWD}

export GOPATH
export GOBIN

all:
	go get -v globster/...

fmt:
	go fmt globster/...

install:
	GOBIN="${OLDBIN}" go install globster

test:
	go test globster/...

clean:
	rm -rf globster pkg

distclean: clean
	rm -rf src/blicky.net

# Yorhel's playground.
play: all
	@./globster -l sock &echo $$!>pid;\
		PERL_ANYEVENT_MODEL=Perl perl -I../tanja/perl ../tanja/perl/tanja-cli.pl -vc $$PWD/sock;\
		kill `cat pid`;\
		rm -f sock pid
